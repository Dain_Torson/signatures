from lib.cuckoo.common.abstracts import Signature
import re

class UsesExeAndDll(Signature):
    name = "system_exe_dll"
    description = "Uses .exe and .dll system files"
    severity = 2
    families = ["virus"]
    authors = ["Ales Veshtort", "Dmitry Tzaregorodcev"]
    minimum = "2.0"
    enable = True

    def on_complete(self):
		files = [file for file in self.get_files() \
			if re.match( r'.*\\system32\\.*(dll|exe)$', file, re.M|re.I)]

		if files:
			for file in files:
				self.mark_ioc("file", file)

			print ("Matched: " + self.description)
			return True
	
		return False
		
