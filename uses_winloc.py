from lib.cuckoo.common.abstracts import Signature

class UsesWinloc(Signature):
    name = "uses_winloc"
    description = "Uses mutex Winloc"
    severity = 2
    families = ["Trojan.Ransome"]
    authors = ["Ales Veshtort", "Dmitry Tzaregorodcev"]
    minimum = "2.0"
    enable = True

    def on_complete(self):
        match = self.check_mutex("WinLoc")

        if match:
            print ("Matched: " + self.description)
            return True

       	return False
        
