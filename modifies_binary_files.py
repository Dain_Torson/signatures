from lib.cuckoo.common.abstracts import Signature

class ModifiesBin(Signature):
    name = "modifies_binary_files"
    description = "Modifies executable on the filesystem"
    severity = 2
    categories = ["generic"]
    authors = ["Dmitry Tsaregorotsev", "Ales Veshtort"]
    minimum = "0.5"

    def complete(self):
        files = [file for file in self.get_files() \
                 if re.match(r'.*(dll|exe)$', file, re.M| re.I)]
        if files.size() > 1:
            return True

        return False
