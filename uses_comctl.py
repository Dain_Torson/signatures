from lib.cuckoo.common.abstracts import Signature

class UsesComctl(Signature):
    name = "uses_comctl"
    description = "Uses comctl32.dll"
    severity = 2
    families = ["spyware"]
    authors = ["Ales Veshtort", "Dmitry Tzaregorodcev"]
    minimum = "2.0"
    enable = True

    def on_complete(self):
        match = self.check_file(pattern=".*\\comctl32.dll$", regex=True)

        if match:
        	print ("Matched: " + self.description)
        	return True

       	return False
        
