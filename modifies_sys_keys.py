from lib.cuckoo.common.abstracts import Signature
import re

class ModifiesSysKeys(Signature):
    name = "modifies_sys_keys"
    description = "Modifies Windows\CurrentSystem keys"
    severity = 2
    families = ["Backdoor"]
    authors = ["Ales Veshtort", "Dmitry Tzaregorodcev"]
    minimum = "2.0"
    enable = True

    def on_complete(self):

        keys = [key for key in self.get_keys() \
            if re.match( r'.*\\Windows.*\\CurrentVersion.*', key, re.M|re.I)]

        if keys:
            print ("Matched: " + self.description)
            return True

       	return False
