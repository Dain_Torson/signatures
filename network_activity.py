from lib.cuckoo.common.abstracts import Signature

class NetworkActivity(Signature):
    name = "network_activity"
    description = "Network activity"
    severity = 2
    families = ["Trojan-Downloader", "Worm"]
    authors = ["Ales Veshtort", "Dmitry Tzaregorodcev"]
    minimum = "2.0"
    enable = True

    def on_complete(self):
		udp = self.get_net_udp()
		dns = self.get_net_domains()
		http = self.get_net_http()

		if udp or dns or http:
			for data in udp:
				self.mark_ioc("udp", str(data))
			for data in dns:
				self.mark_ioc("dns", str(data))
			for data in http:
				self.mark_ioc("http", str(data))

			print ("Matched: " + self.description)
			return True
	
		return False
