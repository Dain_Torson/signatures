from lib.cuckoo.common.abstracts import Signature
import re

class ModifiesCtfKeys(Signature):
    name = "modifies_ctf_keys"
    description = "Modifies Microsoft\CTF key"
    severity = 2
    families = ["Backdoor"]
    authors = ["Ales Veshtort", "Dmitry Tzaregorodcev"]
    minimum = "2.0"
    enable = True

    def on_complete(self):

        keys = [key for key in self.get_keys() \
            if re.match( r'.*\\Microsoft\\CTF.*', key, re.M|re.I)]

        if keys:
            print ("Matched: " + self.description)
            return True

       	return False
