from lib.cuckoo.common.abstracts import Signature

class ModifiesDoc(Signature):
    name = "modifies_documents"
    description = "Modifies documents on the filesystem"
    severity = 2
    categories = ["generic"]
    authors = ["Dmitry Tsaregorotsev", "Ales Veshtort"]
    minimum = "0.5"


    def complete(self):
        files = [file for file in self.get_files() \
                 if re.match(r'.*(doc|png|jpg|gif|wav|txt)$', file, re.M| re.I)]
        if files.size() > 5:
            return True

        return False
