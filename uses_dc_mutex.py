from lib.cuckoo.common.abstracts import Signature
import re

class UsesDCMutex(Signature):
    name = "uses_dc"
    description = "Uses mutex DC"
    severity = 2
    families = ["Dark Commet"]
    authors = ["Ales Veshtort", "Dmitry Tzaregorodcev"]
    minimum = "2.0"
    enable = True

    def on_complete(self):
        mutexes = [mut for mut in self.get_mutexes() \
            if re.match( r'.*DC.*', mut, re.M|re.I)] 

        if mutexes:
            print ("Matched: " + self.description)
            return True

       	return False
        
